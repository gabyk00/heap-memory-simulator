#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 256

#define START_INDEX_OFFSET 4
#define MANAGEMENT_SECTION_SIZE 12
#define NEXT_BLOCK_INDEX_OFFSET 0
#define PREVIOUS_BLOCK_INDEX_OFFSET 4
#define MEMORY_USED_OFFSET 8

typedef unsigned char uchar;

uchar * map;

void initialize(int n) {
    map = (uchar *)calloc(n, sizeof(uchar));
}

void finalize() {
    free(map);
}

int min(int a, int b) {
    return a < b ? a : b;
}

void printLine(int line, int bytes) {
    int offset, bytesOnLine = 16;
    uchar value;
    for (offset = 0; offset < bytes; ++offset) {
        if (offset == bytesOnLine / 2) printf(" ");
        value = *(map + line * bytesOnLine + offset);
        printf("%02X ", value);
    }
}

// Shows the memory map
void dump(int size) {
    int bytesOnLine = 16;
    int currentLine = 0;

    while (size > 0) {
        printf("%08X\t", currentLine * bytesOnLine);
        printLine(currentLine, min(bytesOnLine, size));
        size -= bytesOnLine;
        ++currentLine;
        printf("\n");
    }

    if (size < 0) {
        printf("%08X\n", (currentLine * bytesOnLine + size));
    }
}

int getNextBlockIndex(int blockIndex) {
    return *((int *)((uchar *) (map + blockIndex)));
}

void setNextBlockIndex(int blockIndex, int value) {
    *((int *)((uchar*)(map + blockIndex))) = value;
}

int getPreviousBlockIndex(int blockIndex) {
    return *((int *)((uchar *) (map + blockIndex + PREVIOUS_BLOCK_INDEX_OFFSET)));
}

void setPreviousBlockIndex(int blockIndex, int value) {
    *((int *)((uchar*)(map + blockIndex + PREVIOUS_BLOCK_INDEX_OFFSET))) = value;
}

int getMemoryUsed(int blockIndex) {
    return *((int *)((uchar *) (map + blockIndex + MEMORY_USED_OFFSET)));
}

void setMemoryUsed(int blockIndex, int value) {
    *((int *)((uchar*)(map + blockIndex + MEMORY_USED_OFFSET))) = value;
}

// Writes from index for size bytes in a row the value assigned
void fill(int index, int size, int value) {
    int i, lastIndex = index + size;
    for (i = index; i < lastIndex; ++i) {
        *((uchar *)map + i) = value;
    }
}

int getFreeMemoryIndexRec(int currentIndex, int futureIndex, int totalMemory, int wantedMemory, int align) {
    int offset = 0;
    int freeMemory, memoryUsed, futureFutureIndex;
    int allignedIndex = currentIndex;
    int currentIndexValue = *((int *)((uchar *) map + currentIndex));

    while ((currentIndex + offset + MANAGEMENT_SECTION_SIZE) % align != 0) ++offset;

    if (currentIndex == 0) {
        if (currentIndexValue == 0) {
            freeMemory = totalMemory - (START_INDEX_OFFSET + offset);
        } else {
            freeMemory = currentIndexValue - (START_INDEX_OFFSET + offset);
        }
        memoryUsed = START_INDEX_OFFSET;
    } else {
        memoryUsed = getMemoryUsed(currentIndex);
        if (futureIndex == 0) {
            freeMemory = totalMemory - (currentIndex + memoryUsed);
        } else {
            freeMemory = futureIndex - (currentIndex + memoryUsed);
        }
    }

    if (freeMemory >= wantedMemory + MANAGEMENT_SECTION_SIZE + offset) {
        return currentIndex + memoryUsed + offset;
    } else {
        if (futureIndex == 0) return -1;

        futureFutureIndex = getNextBlockIndex(futureIndex);
        return getFreeMemoryIndexRec(futureIndex, futureFutureIndex, totalMemory, wantedMemory, align);
    }
    return -1;
}

// Returns the index of the next available block of memory for allocation
int getFreeMemoryIndex(int totalMemory, int wantedMemory, int align) {
    int offset = 0;
    int startIndexValue = *((int *)((uchar *)map));
    int freeMemory, freeMemoryIndex;

    if (startIndexValue == 0) {
        while ((START_INDEX_OFFSET + offset + MANAGEMENT_SECTION_SIZE) % align != 0) ++offset;
        freeMemory = totalMemory - (START_INDEX_OFFSET + offset);
        if (freeMemory >= wantedMemory) {
            return START_INDEX_OFFSET + offset;
        }
    } else {
        freeMemoryIndex = getFreeMemoryIndexRec(0, startIndexValue, totalMemory, wantedMemory, align);
        return freeMemoryIndex;
    }
    return -1;
}

// creates a block at memoryIndex and sets the nextBlockIndex, previousBlockIndex and memory used
void createBlock(int memoryIndex, int nextBlockIndex, int previousBlockIndex, int wantedMemory) {
    setNextBlockIndex(memoryIndex, nextBlockIndex);
    setPreviousBlockIndex(memoryIndex, previousBlockIndex);
    setMemoryUsed(memoryIndex, wantedMemory);
}

void manageMemoryAtIndexRec(int currentBlockIndex, int nextBlockIndex, int freeMemoryIndex, int wantedMemory) {
    int nextNextBlockIndex = getNextBlockIndex(nextBlockIndex);

    if (nextBlockIndex == 0) {
        setNextBlockIndex(currentBlockIndex, freeMemoryIndex);
        createBlock(freeMemoryIndex, 0, currentBlockIndex, wantedMemory + MANAGEMENT_SECTION_SIZE);
    } else {
        if (freeMemoryIndex < nextBlockIndex) {
            setNextBlockIndex(currentBlockIndex, freeMemoryIndex);
            setPreviousBlockIndex(nextBlockIndex, freeMemoryIndex);

            createBlock(freeMemoryIndex, nextBlockIndex, currentBlockIndex, wantedMemory + MANAGEMENT_SECTION_SIZE);
        } else {
            manageMemoryAtIndexRec(nextBlockIndex, nextNextBlockIndex, freeMemoryIndex, wantedMemory);
        }
    }

}

// Creates the block at a specified index and changes
// the previous and next blocks accordingly
void manageMemoryAtIndex(int freeMemoryIndex, int wantedMemory) {
    int startIndexValue = *((int *)((uchar*)map));
    int nextBlockIndex, nextNextBlockIndex;

    if (startIndexValue == 0) {
        startIndexValue = freeMemoryIndex;
        *((int *)((uchar *)map)) = startIndexValue;
    }

    if (freeMemoryIndex == startIndexValue) {
        createBlock(freeMemoryIndex, 0, 0, wantedMemory + MANAGEMENT_SECTION_SIZE);
    }
    // if in between index start and firstBlock
    else if (freeMemoryIndex < startIndexValue) {
        nextBlockIndex = startIndexValue;
        setPreviousBlockIndex(nextBlockIndex, freeMemoryIndex);
        setNextBlockIndex(0, freeMemoryIndex);

        createBlock(freeMemoryIndex, nextBlockIndex, 0, wantedMemory + MANAGEMENT_SECTION_SIZE);
    } else {
        nextNextBlockIndex = getNextBlockIndex(nextBlockIndex);
        manageMemoryAtIndexRec(nextBlockIndex, nextNextBlockIndex, freeMemoryIndex, wantedMemory);
    }
}

// Simulates the malloc function
int alloc(int totalMemory, int wantedMemory) {
    return allocAligned(totalMemory, wantedMemory, 1);
}

// Allocates a memory block aligned at an index such as index % align == 0
int allocAligned(int totalMemory, int wantedMemory, int align) {
    int freeMemoryIndex = getFreeMemoryIndex(totalMemory, wantedMemory, align);
    if (freeMemoryIndex != -1) {
        manageMemoryAtIndex(freeMemoryIndex, wantedMemory);
        return freeMemoryIndex + MANAGEMENT_SECTION_SIZE;
    } else {
        return 0;
    }
    return -1;
}

uchar * getStorage(int index, int size) {
    int i;
    uchar * storage = (uchar *)malloc(size * sizeof(uchar));
    for (i = 0; i < size; ++i) {
        storage[i] = *((uchar *)(map + index + i));
    }
    return storage;
}

void setStorage(uchar * storage, int index, int size) {
    int i;
    for (i = 0; i < size; ++i) {
        *((uchar *)(map + index + i)) = storage[i];
    }
}

// Simulates realloc function
int myrealloc(int totalMemory, int index, int wantedMemory) {
    int i;
    int newStorageMemoryIndex;
    uchar * temporaryStorage = getStorage(index, wantedMemory);
    myfree(index);
    newStorageMemoryIndex = alloc(totalMemory, wantedMemory);
    setStorage(temporaryStorage, newStorageMemoryIndex, wantedMemory);
    free(temporaryStorage);
    return newStorageMemoryIndex;
}

// Makes a memory block available for allocation
void myfree(int index) {
    int blockIndex = index - MANAGEMENT_SECTION_SIZE;
    int previousBlockIndex = getPreviousBlockIndex(blockIndex);
    int nextBlockIndex = getNextBlockIndex(blockIndex);


    if (nextBlockIndex != 0) {
        setNextBlockIndex(previousBlockIndex, nextBlockIndex);
        setPreviousBlockIndex(nextBlockIndex, previousBlockIndex);
    } else {
        setNextBlockIndex(previousBlockIndex, 0);
    }
}

void showFreeRec(int index, int totalSize, int * freeBlocks, int * freeBytes) {
    int usedMemory, remainingMemory;
    int nextBlockIndex;

     nextBlockIndex = getNextBlockIndex(index);

    if (nextBlockIndex == 0) {
        if (index == 0) {
            remainingMemory = totalSize - START_INDEX_OFFSET;
        } else {
            usedMemory = getMemoryUsed(index);
            remainingMemory = totalSize - (index + usedMemory);
        }

        if (remainingMemory > 0) {
            ++(*freeBlocks);
            *freeBytes += remainingMemory;
        }
        return;
    }

    if (index == 0) {
        remainingMemory = nextBlockIndex - START_INDEX_OFFSET;
    } else {
        usedMemory = getMemoryUsed(index);
        remainingMemory = nextBlockIndex - (index + usedMemory);
    }

    if (remainingMemory > 0) {
        ++(*freeBlocks);
        *freeBytes += remainingMemory;
    }
    return showFreeRec(nextBlockIndex, totalSize, freeBlocks, freeBytes);
}

int getFree(int index, int totalSize, int * freeBlocks, int * freeBytes) {
    showFreeRec(0, totalSize, freeBlocks, freeBytes);
    return *freeBlocks;
}

// Shows available continuous free memory blocks
void showFree(int size) {
    int freeBlocks = 0, freeBytes = 0;
    getFree(0, size, &freeBlocks, &freeBytes);
    printf("%d blocks (%d bytes) free\n", freeBlocks, freeBytes);
}

void showUsageRec(int index, int totalSize, int * blocks, int * bytes) {
    int nextBlockIndex = getNextBlockIndex(index);
    int usedMemory;

    if (index == 0) {
        if (nextBlockIndex == 0) return;
        return showUsageRec(nextBlockIndex, totalSize, blocks, bytes);
    }
    usedMemory = getMemoryUsed(index);
    ++(*blocks);
    *bytes += usedMemory - MANAGEMENT_SECTION_SIZE;

    if (nextBlockIndex == 0) return;

    return showUsageRec(nextBlockIndex, totalSize, blocks, bytes);
}

// Shows information about the partitioning of the memory
void showUsage(int size) {
    int blocks = 0, bytes = 0, freeBlocks = 0, freeBytes = 0, efficiency, fragmentation;
    freeBlocks = getFree(0, size, &freeBlocks, &freeBytes);
    showUsageRec(0, size, &blocks, &bytes);

    efficiency = 100 * bytes / (START_INDEX_OFFSET + blocks * MANAGEMENT_SECTION_SIZE + bytes);
    fragmentation = blocks == 0 ? 0 : 100 * (freeBlocks - 1) / blocks;

    printf("%d blocks (%d bytes) used\n", blocks, bytes);
    printf("%d%c efficiency\n", efficiency, '%');
    printf("%d%c fragmentation\n", fragmentation, '%');
}

void showAllocationsRec(int index, int freeMemoryStartIndex, int totalSize) {
    int freeMemory, occupiedMemory;
    int nextBlockIndex = getNextBlockIndex(index);

    if (index == 0) {
        if (freeMemoryStartIndex == -1) {
            printf("OCCUPIED 4 bytes\n");
            if (nextBlockIndex == 0) {
                printf("FREE %d bytes\n", totalSize - START_INDEX_OFFSET);
                return;
            }
            return showAllocationsRec(nextBlockIndex, START_INDEX_OFFSET, totalSize);
        } else {
            printf("FREE %d bytes\n", totalSize - freeMemoryStartIndex);
            return;
        }
    }

    freeMemory = index - freeMemoryStartIndex;
    if (freeMemory > 0) {
        printf("FREE %d bytes\n", freeMemory);
    }
    occupiedMemory = getMemoryUsed(index);
    printf("OCCUPIED %d bytes\n", occupiedMemory);

    return showAllocationsRec(nextBlockIndex, index + occupiedMemory, totalSize);
}

// Shows the both used and free blocks of memory
// in order and their size
void showAllocations(int totalSize) {
    showAllocationsRec(0, -1, totalSize);
}

void moveBlock(int currentIndex, int futureIndex) {
    int memoryUsed = getMemoryUsed(currentIndex);
    int * temporaryStorage = getStorage(currentIndex, memoryUsed);
    setStorage(temporaryStorage, futureIndex, memoryUsed);
    free(temporaryStorage);
}

void optimizeFragmentationRec(int totalMemory, int blockIndex, int freeMemoryIndex) {
    if (blockIndex == 0) return;

    int previousBlockIndex = getPreviousBlockIndex(blockIndex);
    int nextBlockIndex = getNextBlockIndex(blockIndex);

    if (freeMemoryIndex > blockIndex)
        return optimizeFragmentationRec(totalMemory, nextBlockIndex, freeMemoryIndex);

    moveBlock(blockIndex, freeMemoryIndex);

    blockIndex = freeMemoryIndex;
    setNextBlockIndex(previousBlockIndex, blockIndex);
    if (nextBlockIndex != 0)
        setPreviousBlockIndex(nextBlockIndex, blockIndex);

    freeMemoryIndex = getFreeMemoryIndex(totalMemory, 1, 1);
    optimizeFragmentationRec(totalMemory, nextBlockIndex, freeMemoryIndex);
}

void optimizeFragmentation(int totalMemory) {
    int firstBlockIndex = *((int *) map);
    int freeMemoryIndex = getFreeMemoryIndex(totalMemory, 1, 1);
    optimizeFragmentationRec(totalMemory, firstBlockIndex, freeMemoryIndex);
}

int main()
{
	int n;
	char buffer[N], *c;
	int size, index, value, align;

   	while (1) {
		fgets(buffer, N, stdin);
		c = strtok(buffer, " ");
		if (strcmp(c, "INITIALIZE") == 0) {
			c = strtok(NULL, " ");
			n = strtol(c, NULL, 10);
			initialize(n);
		}
		if (strcmp(c, "FINALIZE\n") == 0) {
			finalize();
			break;
		}
		if (strcmp(c, "DUMP\n") == 0) {
			dump(n);
		}
		if (strcmp(c, "ALLOC") == 0) {
			c = strtok(NULL, " ");
			size = strtol(c, NULL, 10);
			index = alloc(n, size);
			printf("%d\n", index);
		}
		if (strcmp(c, "REALLOC") == 0) {
            c = strtok(NULL, " ");
            index = strtol(c, NULL, 10);
            c = strtok(NULL, " ");
            size = strtol(c, NULL, 10);
            index = myrealloc(n, index, size);
            printf("%d\n", index);
		}
		if (strcmp(c, "ALLOCALIGNED") == 0) {
            c = strtok(NULL, " ");
            size = strtol(c, NULL, 10);
            c = strtok(NULL, " ");
            align = strtol(c, NULL, 10);
            index = allocAligned(n, size, align);
            printf("%d\n", index);
		}
		if (strcmp(c, "FREE") == 0) {
			c = strtok(NULL, " ");
			index = strtol(c, NULL, 10);
			myfree(index);
		}
		if (strcmp(c, "FILL") == 0) {
			c = strtok(NULL, " ");
			index = strtol(c, NULL, 10);
			c = strtok(NULL, " ");
			size = strtol(c, NULL, 10);
			c = strtok(NULL, " ");
			value = strtol(c, NULL, 10);
			fill(index, size, value);
		}
		if (strcmp(c, "SHOW") == 0) {
			c = strtok(NULL, " ");
			if (strcmp(c, "FREE\n") == 0)
				showFree(n);
			if (strcmp(c, "USAGE\n") == 0)
				showUsage(n);
			if (strcmp(c, "ALLOCATIONS\n") == 0)
				showAllocations(n);
		}
		if (strcmp(c, "OPTIMIZE\n") == 0) {
            optimizeFragmentation(n);
		}

	}
    return 0;
}
