# README #

Simple Heap memory simulator.
To initialize the heap write in the console INITIALIZE N, where N represents the number of bytes you want to have available. When you want to finish write FINALIZE for the program to end.

List of commands:

ALLOC size - makes size bytes available to use and prints the starting index of the allocated memory block

REALLOC index size - reallocates the block of memory from index to the specified                       size

ALLOCALIGNED size align - acts the same as alloc just that the memory is creates at an index shall be aligned at align bytes from the start(index % align = 0)

FREE index - frees the memory

FILL index size value - sets size bytes starting from index to the specified value

DUMP - shows map memory

SHOW FREE - shows the number of free bytes and free blocks of memory

SHOW USAGE - shows information about the memory usage

SHOW ALLOCATIONS - shows the both used and free memory blocks and their size

OPTIMIZE - reduces the memory fragmentation to 0% by aligning the memory in a continuous block